using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    [SerializeField] private float bulletSpeed = 500f;
    [SerializeField] private float lifeDuration = 2f;
    private GameObject officer;
    private float lifeTimer;
    // Start is called before the first frame update
    void Awake()
    {
        officer = GameObject.FindGameObjectWithTag("Officer");
        lifeTimer = lifeDuration;
    }

    // Update is called once per frame
    void Update()
    {
        lifeTimer -= Time.deltaTime; 
        transform.position += transform.forward * bulletSpeed * Time.deltaTime + transform.right * officer.GetComponentInParent<PlayerController>().GetSpeed();
        if (lifeTimer <= 0f)
        {
            Destroy(gameObject);
        }
    }
}
