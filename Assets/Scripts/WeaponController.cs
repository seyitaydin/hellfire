using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    private Touch touch;
    private Vector2 touchPosition;
    private Quaternion rotationY;
    [SerializeField] GameObject officer;
    private Quaternion rotationX;
    [SerializeField] private float speed = 10f;
    
    // Start is called before the first frame update
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Moved)
            {
                float newAngleX = officer.transform.eulerAngles.x - (touch.deltaPosition.y * speed);
                float newAngleY = officer.transform.eulerAngles.y + (touch.deltaPosition.x * speed);
                Debug.Log(newAngleX+"  "+ newAngleY);
                if (newAngleY < 220f)
                {
                    newAngleY = 220f;
                }
                if (newAngleY > 300f)
                {
                    newAngleY = 300f;
                }
                if (newAngleX < 7.5f)
                {
                    newAngleX = 7.5f;
                }
                if (newAngleX > 19.5f)
                {
                    newAngleX = 19.5f;
                }
                officer.transform.eulerAngles = new Vector3 (newAngleX, newAngleY, 0f);
            }
        }
        
    }
}
