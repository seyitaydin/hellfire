using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] GameObject bulletShotLoc;
    [SerializeField] GameObject officer;
    [SerializeField] Camera mainCamera;
    [SerializeField] Camera officerCamera;
    [SerializeField] GameObject laserBeam;
    [SerializeField] ParticleSystem muzzle;
    private float cameraSwitchTime;
    private float oldXAxisPosition;
    private float xAxisSpeed;
    // Start is called before the first frame update
    void Awake()
    {
        oldXAxisPosition = transform.position.z;
        officerCamera.enabled = false;
        mainCamera.enabled = true;
        laserBeam.GetComponent<Renderer>().enabled = false;
        cameraSwitchTime = 5f;

        
    }

    void Start()
    {
        StartCoroutine(TimerCoroutine());
    }

    public float GetSpeed()
    {
        return xAxisSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        float newXAxisPosition = transform.position.z;
        xAxisSpeed =  newXAxisPosition - oldXAxisPosition;
        oldXAxisPosition = newXAxisPosition;

        if ( Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            muzzle.Play();
            laserBeam.GetComponent<Renderer>().enabled = true;
            GameObject bulletObject = Instantiate(bulletPrefab);
            bulletObject.transform.position = new Vector3 (bulletShotLoc.transform.position.x, bulletShotLoc.transform.position.y, bulletShotLoc.transform.position.z + xAxisSpeed );
            bulletObject.transform.forward = bulletShotLoc.transform.forward;

            Debug.Log("Xaxis speed " + xAxisSpeed);

        }
        
    }

    IEnumerator TimerCoroutine()
    {
        yield return new WaitForSeconds(cameraSwitchTime);
        mainCamera.enabled = false;
        officerCamera.enabled = true;
        Debug.Log("Camera switch");
    }


}
